﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceDesk
{
    /// <summary>
    /// Задача
    /// </summary>
    public class Task : Incident, IResponsible, IExecutor
    {
        /// <summary>
        /// ID Инцидента
        /// </summary>
        private int IdIncident;
        /// <summary>
        /// ID приоритета
        /// </summary>
        private int IdPriority;
        /// <summary>
        /// ID состояния
        /// </summary>
        private int IdState;
        /// <summary>
        /// ID ответственного за инцидент
        /// </summary>
        private int IdResponsible;
        /// <summary>
        /// ID исполняющего инцидент
        /// </summary>
        private int IdExecutor;
        /// <summary>
        /// ID Автора инцидента
        /// </summary>
        private int IdAuthor;
        /// <summary>
        /// Дата изменения
        /// </summary>
        private DateTime UpdateDate;
        /// <summary>
        /// Результат по инциденту
        /// </summary>
        private string ResultText;

        public State State
        {
            get => default;
            set
            {
            }
        }

        public Priority Priority
        {
            get => default;
            set
            {
            }
        }

        public void AppointExecutor()
        {
            throw new NotImplementedException();
        }

        public void AppointResponsible()
        {
            throw new NotImplementedException();
        }
    }
}